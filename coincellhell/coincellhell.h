/* * Copyright (c) 2023 Carl Klemm <carl@uvos.xyz>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *  * Neither the name of %ORGANIZATION% nor the names of its contributors may be
 * used to endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <stdint.h>
#include <semaphore.h>
#include <stdbool.h>

#include "usbcommands.h"

/**
API to control EISmultiplexer devices.
* @defgroup API User API
* This API allows you to control the EISmultiplexer device.
* @{
*/

#ifdef __cplusplus
extern "C" {
#endif

struct coincellhell
{
	struct usbshm* priv;
};

struct heater_state
{
	/** true if heater is enabled*/
	bool enabled;
	/** true if heater is close to its set point*/
	bool ready;
	/** true if heater is currently executing a ramp*/
	bool ramp;
	/** true if the system has detected a fault with this heater*/
	bool fault;
	/** the detected fault type as a fault_t enum*/
	fault_t faultType;
	/** current command (0-255) that is being sent to the DAC for this heater*/
	uint8_t dacCommand;
	/** current target temperature for this heater*/
	float setpoint;

	/** if a ramp is currently being executed, its final temperature will be set here*/
	float rampTarget;
	/** if a ramp is currently being executed, the UNIX timestamp at which it was started*/
	time_t rampStartTime;
	/** if a ramp is currently being executed, the UNIX timestamp at it will complete*/
	time_t rampStopTime;
};

/**
 * @brief Attempts to connect to a EISmultiplexer device and initializes a coincellhell struct
 * @param hell pointer to a coincellhell struct to initialize
 * @param serial The serial number of the device to connect to, or 0 for any
 * @return 0 on success and < 0 on failure
 */
int coincellhell_connect(struct coincellhell* hell, uint16_t serial);

/**
 * @brief Reads the current temperature of the given heater at given location
 * @param heater heater from which to get the temperature
 * @param location Place where temperature shall be measured
 * @param temperature A float where the temperature in degrees Celsius will be stored
 * @return 0 on success and < 0 on failure
 */
int coincellhell_get_temperature(struct coincellhell* hell, uint8_t heater, temperature_sensor_location_t location, float* temperature);

/**
 * @brief Sets the target temperature of the given heater
 * @param heater heater for which to set the temperature
 * @param temperature temperature to set
 * @return 0 on success and < 0 on failure
 */
int coincellhell_set_temperature(struct coincellhell* hell, uint8_t heater, float temperature);

/**
 * @brief Gets the target temperature of the given heater
 * @param heater heater for which to set the temperature
 * @param temperature A float where the temperature in degrees Celsius will be stored
 * @return 0 on success and < 0 on failure
 */
int coincellhell_get_temperature_setpoint(struct coincellhell* hell, uint8_t heater, float* temperature);

/**
 * @brief Gets the state struct for the given heater
 * @param heater heater for which to set the temperature
 * @param state A struct where the state will be stored
 * @return 0 on success and < 0 on failure
 */
int coincellhell_get_state(struct coincellhell* hell, uint8_t heater, struct heater_state* state);

/**
 * @brief Sets the enabled state for a give heater
 * @param heater heater for which to set the enabled state
 * @param state A struct where the state will be stored
 * @return 0 on success and < 0 on failure
 */
int coincellhell_set_enabled(struct coincellhell* hell, uint8_t heater, bool enabled);

/**
 * @brief Checks if all temperatures are close to their set points
 * @param ready a pointer to a bool where the result will be stored, true if all temperatures have been reach, false otherwise
 * @return 0 on success and < 0 on failure
 */
int coincellhell_check_ready(struct coincellhell* hell, bool* ready);

/**
 * @brief Will linearly ramp the temperature to the one provided from now until end_time
 * @param heater heater for which to set the ramp
 * @param temperature temperature to ramp to
 * @return 0 on success and < 0 on failure
 */
int coincellhell_set_temperature_ramp(struct coincellhell* hell, uint8_t heater, time_t end_time, float temperature);

/**
 * @brief Cancels any previously set ramp, the set points of the heater will hold the current temperature
 * @param heater heater for which to cancel the ramp
 * @return 0 on success and < 0 on failure
 */
int coincellhell_cancle_ramp(struct coincellhell* hell, uint8_t heater);

/**
 * @brief Turns the led on the PCB on or off
 * @param hell pointer to a coincellhell struct
 * @param on true to turn the led on, false to turn it off
 * @return 0 on success and < 0 on failure
 */
int coincellhell_set_led(struct coincellhell* hell, bool on);

/**
 * @brief resets the device
 * @param hell pointer to a coincellhell struct
 */
int coincellhell_reset(struct coincellhell* hell);

/**
 * @brief Disconnects from the coincellhell
 */
void coincellhell_disconnect(struct coincellhell* hell);

/**
 * @brief Returns a human readable string for a given fault.
 * @param fault the fault code
 * @return a const string describing the fault
 */
const char* coincellhell_string_for_fault(fault_t fault);

int coincellhell_write_eeprom(struct coincellhell* hell, uint16_t addr, uint16_t value);
uint16_t coincellhell_read_eeprom(struct coincellhell* hell, uint16_t addr);
uint8_t coincellhell_read_oscal(struct coincellhell* hell);
uint32_t coincellhell_get_seconds(struct coincellhell* hell);
const uint8_t* coincellhell_get_fw_git_revision(struct coincellhell* hell);
int coincellhell_enable_watchdog(struct coincellhell* hell);
int coincellhell_set_periodic_recal(struct coincellhell* hell, bool recal);
void coincellhell_reset_bus(struct coincellhell* hell);

#ifdef __cplusplus
}
#endif

/**
....
* @}
*/
