/* * Copyright (c) 2023 Carl Klemm <carl@uvos.xyz>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *  * Neither the name of %ORGANIZATION% nor the names of its contributors may be
 * used to endorse or promote products derived from this software without specific
 * prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define _POSIX_C_SOURCE 199309L

#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <assert.h>

#include "coincellhell/coincellhell.h"
#include "usbshm.h"

#define VID 0xfe17
#define PID 0x08dc

static void usleep(uint64_t microseconds)
{
	struct timespec ts;
	ts.tv_sec = microseconds / 1000000;
	ts.tv_nsec = (microseconds % 1000000) * 1000;
	nanosleep(&ts, NULL);
}

int coincellhell_connect(struct coincellhell* hell, uint16_t serial)
{
	if(!hell)
		return -2;
	int ret;
	hell->priv = malloc(sizeof(*hell->priv));
	if(!hell->priv)
		return -1;
	
	ret = usbshm_init(hell->priv, NULL, hell);
	if(ret)
		return -2;

	unsigned char serialStr[5];
	snprintf((char*)serialStr, sizeof(serialStr), "%04hu", serial);

	ret = usbshm_open(hell->priv, VID, PID , serial == 0 ? NULL : serialStr);
	if(ret)
		return -3;
	return 0;
}

int coincellhell_get_temperature(struct coincellhell* hell, uint8_t heater, temperature_sensor_location_t location, float* temperature)
{
	if(!hell || !hell->priv)
		return -2;
	int16_t temperatureRaw = 0;
	uint8_t *dataPtr = (uint8_t*)&temperatureRaw;
	int ret;
	while((ret = usbshm_readControlTransferSync(hell->priv, COMMAND_HEATER_GET_TEMPERATURE, location, heater, dataPtr, 2)) == USBSHM_ERROR_AGAIN)
		usleep(1000000);
	*temperature = temperatureRaw/10.0f;
	return ret == 2 ? 0 : -1;
}

int coincellhell_set_temperature(struct coincellhell* hell, uint8_t heater, float temperature)
{
	if(!hell || !hell->priv)
		return -2;
	if(temperature*10.0f > INT16_MAX || temperature*10.0f < INT16_MIN)
		return -6;
	int ret;
	while((ret = usbshm_writeControlTransfer(hell->priv, COMMAND_HEATER_SET_TEMPERATURE, NULL, 0, temperature*10.0f, heater)) == USBSHM_ERROR_AGAIN)
		usleep(100000);
	return ret >= 0 ? 0 : -1;
}

int coincellhell_get_temperature_setpoint(struct coincellhell* hell, uint8_t heater, float* temperature)
{
	if(!hell || !hell->priv)
		return -2;
	int16_t temperatureRaw = 0;
	uint8_t *dataPtr = (uint8_t*)&temperatureRaw;
	int ret;
	while((ret = usbshm_readControlTransferSync(hell->priv, COMMAND_HEATER_GET_TEMPERATURE_SETPOINT, 0, heater, dataPtr, 2)) == USBSHM_ERROR_AGAIN)
		usleep(100000);
	*temperature = temperatureRaw/10.0f;
	return ret == 2 ? 0 : -1;
}


int coincellhell_get_state(struct coincellhell* hell, uint8_t heater, struct heater_state* state)
{
	uint8_t buf[8];
	int ret;

	if(!hell || !hell->priv)
		return -2;

	while((ret = usbshm_readControlTransferSync(hell->priv, COMMAND_HEATER_GET_STATE, 0, heater, buf, 8)) == USBSHM_ERROR_AGAIN)
		usleep(100000);

	if(ret != 8)
		return -1;

	int16_t* setpoint   = (int16_t*)(buf+1);
	int16_t* rampTarget = (int16_t*)(buf+3);

	state->enabled = buf[0] & (1 << 0);
	state->ready = buf[0] & (1 << 1);
	state->ramp = buf[0] & (1 << 2);
	state->fault = buf[0] & (1 << 3);
	state->faultType = buf[6];

	state->setpoint = *setpoint/10.0f;
	state->rampTarget = *rampTarget/10.0f;
	state->dacCommand = buf[5];

	while((ret = usbshm_readControlTransferSync(hell->priv, COMMAND_HEATER_GET_STATE, 1, heater, buf, 8)) == USBSHM_ERROR_AGAIN)
		usleep(100000);

	if(ret != 8)
		return -1;

	time_t currentTime = time(NULL);
	uint32_t microTime = coincellhell_get_seconds(hell);
	uint32_t* startTime = (uint32_t*)buf;
	uint32_t* endTime = (uint32_t*)(buf+4);

	state->rampStartTime = currentTime + ((int64_t)*startTime - microTime);
	state->rampStopTime = currentTime + ((int64_t)*endTime - microTime);

	return 0;
}

int coincellhell_set_enabled(struct coincellhell* hell, uint8_t heater, bool enabled)
{
	int ret;

	if(!hell || !hell->priv)
		return -2;

	while((ret = usbshm_writeControlTransfer(hell->priv, COMMAND_HEATER_SET_ENABLED, NULL, 0, enabled, heater)) == USBSHM_ERROR_AGAIN)
		usleep(100000);
	return ret;
}

int coincellhell_check_ready(struct coincellhell* hell, bool* ready)
{
	*ready = false;
	int ret;
	uint8_t readybits;

	if(!hell || !hell->priv)
		return -2;

	while((ret = usbshm_readControlTransferSync(hell->priv, COMMAND_READY, 0, 0, &readybits, 1)) == USBSHM_ERROR_AGAIN)
		usleep(100000);
	*ready = !(~readybits & 0x0F);
	return ret == 1 ? 0 : -1;
}

int coincellhell_set_temperature_ramp(struct coincellhell* hell, uint8_t heater, time_t end_time, float temperature)
{
	time_t currentTime = time(NULL);
	time_t timeDelta = end_time - currentTime;

	if(timeDelta < 0)
		return -1;
	if(!hell || !hell->priv)
		return -2;

	char buffer[7];
	buffer[0] = heater;

	uint32_t microTime = coincellhell_get_seconds(hell);
	uint32_t* endTime = (uint32_t*)(buffer+3);
	int16_t* setpoint = (int16_t*)(buffer+1);
	*endTime = microTime + timeDelta;
	*setpoint = temperature/10.0f;

	int ret;
	while((ret = usbshm_writeControlTransfer(hell->priv, COMMAND_HEATER_SETUP_RAMP, buffer, 7, 0, 0)) == USBSHM_ERROR_AGAIN)
		usleep(100000);

	return ret;
}

int coincellhell_cancle_ramp(struct coincellhell* hell, uint8_t heater)
{
	int ret;

	if(!hell || !hell->priv)
		return -2;

	while((ret = usbshm_writeControlTransfer(hell->priv, COMMAND_HEATER_RAMP_CANCLE, NULL, 0, 0, heater)) == USBSHM_ERROR_AGAIN)
		usleep(100000);
	return ret;
}

int coincellhell_set_led(struct coincellhell* hell, bool on)
{
	int ret;

	if(!hell || !hell->priv)
		return -2;

	while((ret = usbshm_writeControlTransfer(hell->priv, on ? COMMAND_LED_ON : COMMAND_LED_OFF, NULL, 0, 0, 0)) == USBSHM_ERROR_AGAIN)
		usleep(100000);
	return ret;
}

int coincellhell_write_eeprom(struct coincellhell* hell, uint16_t addr, uint16_t value)
{
	int ret;

	if(!hell || !hell->priv)
		return -2;

	while((ret = usbshm_writeControlTransfer(hell->priv, COMMAND_WRITE_EEPROM, NULL, 0, value, addr)) == USBSHM_ERROR_AGAIN)
		usleep(100000);
	return ret;
}

uint16_t coincellhell_read_eeprom(struct coincellhell* hell, uint16_t addr)
{
	uint8_t buffer[2] = {};

	if(!hell || !hell->priv)
		return 0;

	usbshm_readControlTransferSync(hell->priv, COMMAND_READ_EEPROM, 0, addr, buffer, 2);
	return *((uint16_t*)buffer);
}


uint8_t coincellhell_read_oscal(struct coincellhell* hell)
{
	uint8_t oscal;

	if(!hell || !hell->priv)
		return 0;

	usbshm_readControlTransferSync(hell->priv, COMMAND_READ_OSCAL, 0, 0, &oscal, 1);
	return oscal;
}

uint32_t coincellhell_get_seconds(struct coincellhell* hell)
{
	uint32_t seconds;

	if(!hell || !hell->priv)
		return 0;

	usbshm_readControlTransferSync(hell->priv, COMMAND_GET_SECONDS, 0, 0, (uint8_t*)&seconds, 4);
	return seconds;
}

const uint8_t* coincellhell_get_fw_git_revision(struct coincellhell* hell)
{
	static uint8_t gitrev[9] = {0};

	if(!hell || !hell->priv)
		return gitrev;

	usbshm_readControlTransferSync(hell->priv, COMMAND_GET_FIRMWARE_GITREV, 0, 0, gitrev, 8);
	return gitrev;
}

int coincellhell_reset(struct coincellhell* hell)
{
	if(!hell || !hell->priv)
		return -1;
	usbshm_reopen(hell->priv);
	for(int i = 0; i < 4; ++i)
	{
		int ret = coincellhell_set_enabled(hell, 0, false);
		if(ret != 0)
			return ret;
	}
	return 0;
}


int coincellhell_enable_watchdog(struct coincellhell* hell)
{
	int ret;
	while((ret = usbshm_writeControlTransfer(hell->priv, COMMAND_ENABLE_WATCHDOG, NULL, 0, 0, 0)) == USBSHM_ERROR_AGAIN)
		usleep(100000);
	return ret;
}

int coincellhell_set_periodic_recal(struct coincellhell* hell, bool recal)
{
	int ret;
	while((ret = usbshm_writeControlTransfer(hell->priv, COMMAND_ENABLE_WATCHDOG, NULL, 0, recal, 0)) == USBSHM_ERROR_AGAIN)
		usleep(100000);
	return ret;
}

void coincellhell_reset_bus(struct coincellhell* hell)
{
	usbshm_reset(hell->priv);
}

void coincellhell_disconnect(struct coincellhell* hell)
{
	if(!hell || !hell->priv)
		return;
	usbshm_distroy(hell->priv);
	free(hell->priv);
	hell->priv = NULL;
}

const char* coincellhell_string_for_fault(fault_t fault)
{
	switch(fault)
	{
		case FAULT_NONE:
			return "no fault detected";
		case FAULT_FRONT_SENSOR:
			return "unable to read from front sensor";
		case FAULT_SIDE_SENSOR:
			return "unable to read from side sensor";
		case FAULT_OVERTEMP:
			return "system shutdown due to a overtemperature event";
		case FAULT_UNDERTEMP:
			return "system shutdown due to a undertemperature event";
		case FAULT_UNSPECIFIED:
		default:
			return "a unspecified fault occured";
	}
}


